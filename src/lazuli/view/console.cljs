(ns lazuli.view.console
  (:require #_[repl-tooling.editor-integration.renderer.console :as console]
            #_[repl-tooling.editor-integration.renderer :as render]
            #_[repl-tooling.editor-integration.configs :as config]
            #_[repl-tooling.eval :as eval]
            [saphire.ui.console :as saphire-console]
            [tango.ui.console :as console]
            [saphire.ruby-parsing :as rp]
            [promesa.core :as p]
            [clojure.edn :as edn]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(defn register-console! [state]
  (let [console (console/view "lazuli")]
    (.. js/document
        (querySelector "div")
        (replaceWith console))
    (swap! state assoc-in [:editor/callbacks :get-console] (constantly console))
    (saphire-console/prepare! console state)))

(def ^:private pending-evals (atom {}))
(defonce ^:private post-message* (-> (js/acquireVsCodeApi) .-postMessage))

(defn- post-message! [message]
  (post-message* (pr-str message)))

#_
(defrecord Evaluator [flavor]
  eval/Evaluator
  (evaluate [_ command _ callback]
    (let [id (gensym)]
      (swap! pending-evals assoc id callback)
      (post-message! (pr-str {:op :eval-code
                              :args {:command (str "(do\n" command "\n)")
                                     :repl flavor
                                     :id id}}))
      id)))

(defn- to-edn [string]
  (let [edn (edn/read-string {:default tagged-literal} string)
        txt (:as-text edn)
        key (if (:error edn) :error :result)]
    (-> edn
        (dissoc :parsed?)
        (assoc key txt))))

(defonce pending-calls (atom {}))
(defn- run-call! [op cmd args]
  (let [id (gensym)
        prom (p/deferred)]
    (swap! pending-calls assoc id prom)
    (post-message! (pr-str {:op op :cmd cmd :args args :id id}))
    prom))

(defn- run-call-args! [callback]
  (fn [ & args] (run-call! :run-callback callback args)))

(def editor-state
  {:run-callback (fn [cmd & args] (run-call! :run-callback cmd args))
   :run-feature (fn [cmd & args] (run-call! :run-feature cmd args))
   :editor/callbacks {:file-exists (run-call-args! :file-exists)
                      :read-file (run-call-args! :read-file)
                      :open-editor (run-call-args! :open-editor)
                      ; :config-file-path (run-call-args! :config-file-path)
                      ; :register-commands (run-call-args! :register-commands)
                      :on-start-eval (run-call-args! :on-start-eval)
                      :on-eval (run-call-args! :on-eval)
                      :editor-data (run-call-args! :editor-data)
                      :notify (run-call-args! :notify)
                      :get-config (run-call-args! :get-config)
                      :prompt (run-call-args! :prompt)
                      :on-copy (run-call-args! :on-copy)
                      :on-stdout (run-call-args! :on-stdout)
                      :on-stderr (run-call-args! :on-stderr)
                      :on-result (run-call-args! :on-result)
                      :get-rendered-results (run-call-args! :get-rendered-results)
                      :on-patch (run-call-args! :on-patch)}})

(defn- render-result [string-result repl-flavor]
  #_
  (let [repl (->Evaluator repl-flavor)
        result (to-edn string-result)]
    (console/result result #(render/parse-result % repl (r/atom editor-state)))))

(defn- send-response! [{:keys [id result]}]
  (let [callback (get @pending-evals id)]
    (callback result))
  (swap! pending-evals dissoc id))

#_
(defn- find-patch [id maybe-coll]
  (let [elem (if (instance? reagent.ratom/RAtom maybe-coll)
               (dissoc @maybe-coll :editor-state :repl)
               maybe-coll)]
    (if (and (instance? render/Patchable elem)
             (= id (:id elem)))
      maybe-coll
      (when (coll? elem)
        (->> elem
             (map #(find-patch id %))
             flatten
             (filter identity))))))

#_
(defn- patch-result! [{:keys [id result]}]
  (let [repl (->Evaluator :cljs)
        norm {:result (:as-text result)
              :as-text (:as-text result)}
        results (->> @console/out-state
                     (filter #(-> % first (= :result)))
                     (map second))]
    #_
    (doseq [result (find-patch id results)]
      (swap! result assoc :value (render/parse-result norm repl (r/atom editor-state))))))

(defn- resolve-prom! [{:keys [id result]}]
  (when-let [prom (get @pending-calls id)]
    (swap! pending-calls dissoc id)
    (p/resolve! prom result)))

(def ^:private readers
  {'saphire.ruby-parsing.RubySet rp/map->RubySet
   'saphire.ruby-parsing.RubyMap rp/map->RubyMap
   'saphire.ruby-parsing.RubyVector rp/map->RubyVector
   'saphire.ruby-parsing.RubyInstanceVar rp/map->RubyInstanceVar
   'saphire.ruby-parsing.RubyNumber rp/map->RubyNumber
   'saphire.ruby-parsing.RubyKeyword rp/map->RubyKeyword
   'saphire.ruby-parsing.RubyString rp/map->RubyString
   'saphire.ruby-parsing.RubyObject rp/map->RubyObject
   'saphire.ruby-parsing.RubyVariable rp/map->RubyVariable
   'saphire.ruby-parsing.RubyUnknownVal rp/map->RubyUnknownVal})

(defn- update-traces! [state traces]
  (swap! state assoc :repl/tracings traces)
  (saphire-console/update-traces! state))

(defn- clear-watches! [^js console]
  (set! (.. console (querySelector ".watches") -innerHTML) ""))

(defn listen-to-event! [state message]
  (let [{:keys [command obj]} (->> message .-data (edn/read-string {:readers readers}))
        get-console (-> @state :editor/callbacks :get-console)]
    (def obj obj)
    (case command
      :update-traces (update-traces! state obj)
      :get-config (swap! state assoc-in [:editor/callbacks :get-config] (constantly obj))
      :out (console/stdout (get-console) obj)
      :err (console/stderr (get-console) obj)
      :clear-watches (clear-watches! (get-console))
      :update-watch (saphire-console/update-watch! state obj)
      (prn :UNKNOWN command))))

(defn main []
  (let [state (atom {:editor/callbacks {:open-editor #(post-message! {:op :open-editor :args %})
                                        :get-config (constantly {})}})]
    (def state state)
    (.. js/window
        (addEventListener "message" #(listen-to-event! state %)))
    (post-message! {:op :get-config})
    (register-console! state)))
