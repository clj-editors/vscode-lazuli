(ns lazuli.ui
  (:require [clojure.edn :as edn]
            [promesa.core :as p]
            #_[repl-tooling.editor-integration.connection :as connection]
            #_[repl-tooling.eval :as eval]
            [lazuli.state :as st]
            ["vscode" :refer [Uri] :as vscode]
            ["path" :as path]))

(defonce view (atom nil))
(defonce curr-dir (atom nil))

(def style
  "
body {
  height: 100vh;
}

.lazuli.result {
	overflow: auto;
}

.lazuli.result,
.lazuli.console {
	display: flex;
	white-space: pre;
	user-select: auto;
	padding: 0.2em;
	padding-left: 1em;
	border-radius: 3px;
}

.lazuli.console .input-text {
  flex-grow: 1;
}

.lazuli.result div.italic,
.lazuli.console div.italic {
	font-style: italic;
}

.lazuli.result div.traces,
.lazuli.console div.traces {
	max-height: 10em;
	overflow: auto;
}

.lazuli.result code,
.lazuli.console code {
	font-size: var(--editor-font-size);
	font-family: var(--editor-font-family);
	color: var(--vscode-icon-foreground);
	background: rgba(0, 0, 0, 0.3);
}

.lazuli.result div.italic,
.lazuli.console div.italic {
	font-style: italic;
}

.lazuli.result div.cell,
.lazuli.console div.cell {
	display: flex;
}

.lazuli.result div.cell div.content,
.lazuli.console div.cell div.content {
	display: flex;
}

.lazuli.result atom-text-editor,
.lazuli.console atom-text-editor {
	background-color: rgba(0, 0, 0, 0.08);
}

.lazuli.result atom-text-editor .scrollbar-corner,
.lazuli.console atom-text-editor .scrollbar-corner {
	display: none;
}

.lazuli.result atom-text-editor .cursor,
.lazuli.console atom-text-editor .cursor {
	display: none;
}

.lazuli.result div.text,
.lazuli.console div.text {
	white-space: pre-wrap;
	display: block;
	width: 100%;
}

.lazuli.result div.markdown,
.lazuli.console div.markdown {
	display: block;
	white-space: normal;
	width: 100%;
}

.lazuli.result a,
.lazuli.console a {
	color: var(--vscode-icon-foreground);
}

.lazuli.result div.error,
.lazuli.console div.error {
	display: flex;
	color: var(--vscode-errorForeground);
}

.lazuli.result div.error div.map-key.opened,
.lazuli.console div.error div.map-key.opened {
	display: flex;
	color: green;
	font-weight: bolder;
}

.lazuli.result div.title,
.lazuli.console div.title {
	display: flex;
	text-shadow: 1px 1px;
}

.lazuli.result div.children,
.lazuli.console div.children {
	display: flex;
	padding-left: 1em;
	flex-direction: column;
}

.lazuli.result div.cols,
.lazuli.console div.cols {
	display: flex;
}

.lazuli.result div.rows,
.lazuli.console div.rows {
	display: flex;
	flex-direction: column;
}

.lazuli.result div.map-key.opened,
.lazuli.console div.map-key.opened {
	display: flex;
	color: #090;
}

.lazuli.result div.space,
.lazuli.console div.space {
	opacity: 0.1;
	margin: 0.6em;
}

.lazuli.result a.chevron,
.lazuli.console a.chevron {
	font-family: 'Octicons Regular';
	font-weight: bold;
	font-size: var(--editor-font-size);
	padding-right: 0.6em;
}

.lazuli.result a.chevron:focus,
.lazuli.console a.chevron:focus {
	text-decoration: none;
}

.lazuli.result a.chevron.closed::before,
.lazuli.console a.chevron.closed::before {
	content: \"\\f078\";
}

.lazuli.result a.chevron.opened::before,
.lazuli.console a.chevron.opened::before {
	content: \"\\f0a3\";
}

.lazuli.result a.more-info::after,
.lazuli.console a.more-info::after {
	content: \"more\";
}

.lazuli.result select,
.lazuli.console select {
	padding: 0.2em;
}

.lazuli.result .full-content,
.lazuli.console .full-content {
	padding-top: 1em;
	padding-bottom: 0.5em;
	overflow: scroll;
	display: block;
	width: 100%;
	height: 100%;
}

.lazuli.result .full-content .content-part,
.lazuli.console .full-content .content-part {
	padding: 0em;
	overflow: visible;
	display: block;
	width: auto;
	height: auto;
	overflow-anchor: none;
}

.lazuli.result .full-content .scroll-anchor,
.lazuli.console .full-content .scroll-anchor {
	overflow-anchor: auto;
	height: 0.5em;
}

.tango.icon-container {
	display: flex;
	min-width: 1.2em;
	min-height: 1.2em;
}

.tango.icon-container .icon {
	white-space: pre;
	font-family: 'Octicons Regular';
	display: inline-block;
	margin: 0;
	padding: 0;
}

.tango.icon-container .icon:before {
	margin: 0;
}

.tango.icon-container .icon.loading {
	animation: spin 4s linear infinite;
	opacity: 0.7;
	margin: auto;
	line-height: 1;
}

.tango.icon-container .icon.loading:before {
	content: \"\\f02f\";
	line-height: 1;
}

.tango.icon-container .icon.check:before {
	content: \"\\f03a\";
}

.tango.icon-container .icon.stop:before {
	content: \"\\f08f\";
}

div.tango.console {
	display: flex;
	flex-direction: column;
	width: 100%;
	height: 100%;
	padding-bottom: 0;
	padding-right: 0;
	color: var(--vscode-editor-foreground);
	font-family: var(--vscode-editor-font-family);
	font-size: var(--vscode-editor-font-size);
}

div.tango.console div.details {
	border-bottom: 2px solid;
	margin-right: 0.5em;
}

div.tango.console div.details .shadow-cljs {
	display: flex;
	flex-direction: column;
}

div.tango.console a {
	color: var(--vscode-icon-foreground);
}

div.tango.console .output {
	white-space: pre-wrap;
	word-wrap: anywhere;
	flex-direction: column;
}

div.tango.console .items {
	margin-top: 5px;
	margin-bottom: 10px;
}

div.tango.console .cell .gutter {
	text-align: center;
}

div.tango.console .cell .gutter .icon {
	transition: all 0.1s ease;
	width: 2em;
}

div.tango.console .cell .gutter .icon:before {
	font-size: inherit;
}

div.tango.console .cell .content {
	padding-right: 20px;
	border-bottom: 1px solid rgba(171, 178, 191, 0.25);
	padding-top: 4px;
	padding-bottom: 10px;
	margin-right: 1em;
	flex-grow: 1;
}

div.tango.console .cell .content.out span,
div.tango.console .cell .content.err span {
	display: contents;
	white-space: pre-wrap;
	word-break: break-word;
}

.tango.console input {
	font-size: 0.7em;
	background-color: var(--vscode-editor-foreground);
}

.tango.console .watches {
	max-height: 20em;
	overflow: scroll;
	margin-bottom: 0.5em;
}

.tango.console .traces {
	white-space: normal;
	margin-bottom: 0.5em;
}
")

(defn- do-render-console! [^js webview curr-dir]
  (let [js-path (. path join curr-dir "view" "js" "main.js")
        font-path (. path join curr-dir "view" "octicons.ttf")
        res-js-path (.. webview -webview (asWebviewUri (. Uri file js-path)))
        res-font-path (.. webview -webview (asWebviewUri (. Uri file font-path)))]
    (set! (.. webview -webview -html) (str "<html>
<head>
  <style>
@font-face {
  font-family: 'Octicons Regular';
  src: url(" res-font-path ");
}
" style "
  </style>
</head>
<body>
  <div></div>
  <script type='text/javascript' src='" res-js-path "'></script>
</body>
</html>"))))

(defn post-message! [^js view message]
  (some-> view
          .-webview
          (.postMessage (pr-str message))))

(defn- evaluate! [{:keys [command repl id]}]
  #_
  (when-let [evaluator (if (= repl :clj)
                         (st/repl-for-clj))]
    #_
    (eval/evaluate evaluator command {:ignore true}
                   (fn [result]
                     (post-message! {:command :eval-result
                                     :obj {:result result :id id}})))))

#_
(defn- resolve-result! [id call cmd args]
  (p/let [result (apply call cmd args)]
    (post-message! {:command :call-result
                    :obj {:result result :id id}})))

(defn- handle-message [state {:keys [op args cmd id]}]
  (prn :RECEIVED op)
  (let [get-console (-> @state :editor/callbacks :get-console)
        calls (:editor/callbacks @state)]
    (case op
      :get-config (post-message! (get-console) {:command :get-config :obj ((:get-config calls))})
      :open-editor ((:open-editor calls) args)
      (prn :unknown-cmd)))
  #_
  (let [{:keys [run-callback run-feature]} @editor-state]
    (case op
      :eval-code (evaluate! args)
      :run-callback (resolve-result! id run-callback cmd args)
      :run-feature (resolve-result! id run-feature cmd args))))

(defn- listen-to-events! [^js view state]
  (.. view (onDidDispose #((-> @state :editor/commands :disconnect :command))))
  (.. view -webview (onDidReceiveMessage (comp #(handle-message state %) edn/read-string))))

(defn create-console! [state]
  (let [res-path (.. Uri (file (. path join @curr-dir "view")))]
    (reset! view (.. vscode -window
                     (createWebviewPanel "lazuli-console"
                                         "Lazuli REPL"
                                         (.. vscode -ViewColumn -Two)
                                         (clj->js
                                          (cond-> {:enableScripts true
                                                   :retainContextWhenHidden true
                                                   :localResourceRoots [res-path]}

                                            js/goog.DEBUG
                                            (assoc
                                             :portMapping [{:extensionHostPort 9699
                                                            :webviewPort 9699}]))))))

    (do-render-console! @view @curr-dir)
    (listen-to-events! @view state)
    @view))

(defn send-output! [view stream text]
  (post-message! view {:command stream :obj text}))

(defn did-eval! [view result]
  (post-message! view {:command :did-eval
                       :obj (pr-str result)}))

(defn will-eval! [view result]
  (post-message! view {:command :will-eval
                       :obj (pr-str result)}))

(defn clear-console! [view]
  (post-message! view {:command :clear}))
