(ns lazuli.commands.connection
  (:require #_[repl-tooling.editor-integration.connection :as connection]
            #_[repl-tooling.editor-integration.configs :as configs]
            #_[repl-tooling.editor-helpers :as helpers]
            [tango.editor-helpers :as helpers]
            [saphire.connections :as s-connections]
            [tango.ui.console :as tango-console]
            [lazuli.vs :as vs]
            [lazuli.aux :as aux :include-macros true]
            [lazuli.ui :as ui]
            [lazuli.state :as state]
            [clojure.string :as str]
            [promesa.core :as p]
            ["vscode" :as vscode :refer [Task TaskDefinition]]
            ["path" :as path]
            ["os" :as os]
            ["fs" :refer [existsSync readFileSync] :as fs]))

(defn- create-pseudo-terminal [command]
  (let [emitter (-> vscode .-EventEmitter new)]
    (new (. vscode -CustomExecution)
      (fn [_]
        #js {:onDidClose (.-event emitter)
             :onDidWrite identity
             :close identity
             :handleInput identity
             :open (fn [& _]
                     (command)
                     (.fire ^js emitter))
             :setDimensions identity}))))

(defn- create-task [command-key command-name function]
  (doto (new Task
          #js {:type "lazuli" :command (name command-key)}
          (.. vscode -TaskScope -Workspace)
          command-name
          "lazuli"
          (create-pseudo-terminal function))
        (aset "presentationOptions"
              #js {:reveal (.. vscode -TaskRevealKind -Never)
                   :clear true})))

(defonce custom-commands (atom {}))

(def provider
  #js {:provideTasks (fn [_]
                       (->> @custom-commands
                            (map (fn [[k v]]
                                   (create-task k (:name v) (:command v))))
                            clj->js))
       :resolveTask (fn [_ _])}

; (defn- register-console! []
;   (ui/create-console!)
  #_
  (aux/add-transient! (.. vscode
                          -commands
                          (registerCommand "lazuli.clear-console"
                                           ui/clear-console!))))

(defn- disconnect! []
  (some-> ^js @ui/view .dispose)
  (reset! ui/view nil)
  (when-not (empty? @state/state)
    (aux/clear-commands!)
    (aux/clear-transients!)
    (swap! state/state dissoc :state)
    (vs/info "Disconnected from REPLs")))

(defn- folders []
  (->> (.. vscode -workspace -workspaceFolders)
       (map #(-> % .-uri str (str/replace #"file://" "")))
       vec))

(defn- config-dir []
  (let [config (-> vscode
                   .-workspace
                   (.getConfiguration "lazuli")
                   .-configFile
                   not-empty
                   (or (path/join "$HOME" ".config" "lazuli" "config.cljs")))]
    (str/replace config #"\$HOME" (.homedir os))))

(defn- get-config []
  {:project-paths (folders)
   :max-traces 4000
   :eval-mode :clj})

(defn- notify! [{:keys [type title message]}]
  (let [txt (cond-> title message (str ": " message))]
    (case type
      :info (vs/info txt)
      :warning (vs/warn txt)
      :error (vs/error txt))))

(def ^:private original-cmds
  #{"evaluate-line"
    "evaluate-top-block"
    "evaluate-selection"
    "clear-console"
    "disconnect"
    "load-file"
    "load-file-and-inspect"
    "break-evaluation"
    "doc-for-var"
    "run-tests-in-ns"
    "run-test-for-var"
    "source-for-var"
    "open-config"
    "go-to-var-definition"})

(defn- observe-editor! [state ^js editor]
  (when editor
    (when-let [display! (-> @state :editor/features :display-watches)]
      (when-let [path (not-empty (.. editor -document -uri -fsPath))]
        (display! path)))))

(defn- register-commands! [console commands state]
  (aux/clear-commands!)
  (aux/add-command!
   (.. vscode -window (onDidChangeActiveTextEditor #(observe-editor! state %))))
  (reset! custom-commands {})

  (doseq [[k command] commands]
    (if (original-cmds (name k))
      (aux/add-command!
       (.. vscode
           -commands
           (registerCommand (str "lazuli." (name k)) (:command command))))
      (swap! custom-commands assoc k command))))

(defn- open-editor [{:keys [file-name line column]}]
  (p/let [doc (.. vscode -workspace (openTextDocument file-name))
          editor (.. vscode
                     -window
                     (showTextDocument doc #js {:viewColumn (.. vscode -ViewColumn -One)
                                                :preview true}))

          Sel (.. vscode -Selection)
          selection (new Sel
                      (or line 0) (or column 0)
                      (or line 0) (or column 0))]
    (aset editor "selection" selection)
    (.revealRange ^js editor selection)))

(defn- mkdir-p [directory]
  (let [parent (path/dirname directory)]
    (when-not (fs/existsSync directory)
      (when-not (fs/existsSync parent)
        (mkdir-p parent))
      (fs/mkdirSync directory))))

(defn- prepare-config! []
  (let [config-file (config-dir)]
    (when-not (fs/existsSync config-file)
      (try
        (mkdir-p (path/dirname config-file))
        (fs/closeSync (fs/openSync config-file "w"))
        (catch :default _)))))

(defn- update-traces [console state]
  ; So... stupid VSCode API separates webviews, so I have to POST a MESSAGE to UPDATE an UI....
  ; Yeah... it's dumb...
  (let [old-timeout (:repl/tracings-timeout @state)]
    (js/clearTimeout old-timeout)
    (swap! state assoc :repl/tracings-timeout
           (js/setTimeout #(ui/post-message! @console {:command :update-traces
                                                       :obj (:repl/tracings @state)})
                          100))))

(defn- connect-nrepl [[host port]]
  (prepare-config!)
  (p/let [console (atom nil)
          callbacks {:on-disconnect disconnect!
                     ;; FIXME - move everything that uses @console to a different callback?
                     :on-start-eval #(ui/will-eval! @console %)
                     :on-eval #(ui/did-eval! @console %)
                     :register-commands register-commands!
                     :get-rendered-results #(tango-console/all-parsed-results @console)
                     :notify notify!
                     :on-stdout #(ui/send-output! @console :out %)
                     :on-stderr #(ui/send-output! @console :err %)
                     :open-editor open-editor
                     :prompt (partial prn :PROMPT)
                     :get-config #(get-config)
                     :editor-data #(vs/get-editor-data)
                     :update-traces #(update-traces console %)
                     :clear-watches #(ui/post-message! @console {:command :clear-watches})
                     :update-watch #(ui/post-message! @console {:command :update-watch
                                                                :obj %2})
                     :config-directory (path/dirname (config-dir))}
          repl-state (s-connections/connect-nrepl! host port callbacks
                                                   {:open-console ui/create-console!})]
    (when repl-state
      (reset! console ((-> @repl-state :editor/callbacks :get-console)))
      ; (reset! lazuli-complete/state repl-state)
      ; (reset! symbols/find-symbol (-> @repl-state :editor/features :find-definition))
      (swap! state/state assoc :state repl-state))))

(defn- extract-host-port [txt]
  (let [[host port] (str/split txt #":")
        port (js/parseInt port)]
    (if (js/isNaN port)
      (do (vs/error "Port must be a number") nil)
      (do
        (swap! state/state assoc :old-conn-info txt :old-port port)
        [host port]))))

(defn connect!
  ([host port]
   (if (state/repl-for-clj)
     (vs/warn "REPL is already connected")
     (connect-nrepl [host port])))
  ([]
   (let [port (helpers/get-possible-port (folders) (:old-port @state/state))
         conn-info (if port
                     (str "localhost:" port)
                     (:old-conn-info @state/state "localhost:"))]
     (if (:state @state/state)
       (vs/warn "REPL is already connected")
       (-> (vs/prompt "Connect to Clojure: inform <host>:<port>" conn-info)
           (p/then extract-host-port)
           (p/then #(some-> % connect-nrepl)))))))
